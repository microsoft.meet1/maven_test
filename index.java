package com.example.demo.controller;

import org.springframwork.web.bind.annotation.GetMapping;
import org.springframwork.web.bind.annotation.RestController;


@RestController
public class Index {
	
		@GetMapping("/")
		public String home(){
			return "hello world";
		}
}